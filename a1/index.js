// 1--> ********* done **********
//How do you create arrays in JS?
//---> by assigning an array to a variable
const songs = ["Afterglow", "Perfect"];
console.log(songs)
//---> by using the "new" keyword & the Array() constructor
new Array(2) // (2) [empty × 2]   
// new Array(2)[0]   //returns new array of 2 empty slots
// undefined
let myFaveSingers = new Array("John Mayer", "Ed Sheeran")
console.log(myFaveSingers) // (2) ['John Mayer', 'Ed Sheeran']
console.log(myFaveSingers.length) // 2
console.log(myFaveSingers[0]) // 'John Mayer'

//--->by using Array.of() 
let comfortFoods = Array.of("lechon", "kare-kare")
console.log(comfortFoods) 
console.log(comfortFoods.length) // 2
console.log(comfortFoods[0])
let newArr = Array.of(10)
console.log(newArr) 
//[10]0: 10          // returns an array with '10' in it
//length: 1
//[[Prototype]]: Array(0)
// ******************************************************************


// 2--> ********* done **********
//How do you access the first character of an array? through 
//--->through the index number of its position in the array,[0]
let country = 'China'
country[0]   // 'C'
// ******************************************************************


// 3--> ********* done **********
//How do you access the last character of an array?
//--->elements of an array are like characters in a string, so, to access the last character, just subtract
//    1 from the string's length since the index starts at 0
let message = "rainbow"
let lastLetterOfMessage = message[message.length - 1]
console.log(lastLetterOfMessage)
// w
// ******************************************************************


// 4---> ********* done **********
//What array method searches for, and returns the index of a given value in an array? This method returns 
//-1 if given value is not found in the array.
//---> indexOf() method 
// ******************************************************************


// 5---> ********* done **********
//What array method loops over all elements of an array, performing a user-defined function on each 
//iteration?
//---> forEach() method
// ******************************************************************


// 6---> ********* done **********
//What array method creates a new array with elements obtained from a user-defined function?
//---> Array.from() method
// ******************************************************************


// 7---> ********* done **********
//What array method checks if all its elements satisfy a given condition?
//---> every() method
// ******************************************************************


// 8--->
//What array method checks if at least one of its elements satisfies a given condition?
//---> some() method
// ******************************************************************

// 9---> ********* done **********
//True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
//--->FALSE; we can replace, insert or delete using this method, so contents of original array will 
//    definitely be changed
let dogs = ["Alex", "Dion", "Goop"]
dogs.splice(1, 0, "Oscar");
console.log(dogs) // (4) ['Alex', 'Oscar', 'Dion', 'Goop']
dogs.splice(2, 1);
console.log(dogs) // (3) ['Alex', 'Oscar', 'Goop']
dogs.splice(1, 2, "Pete", "Grizzly");
console.log(dogs) // (3) ['Alex', 'Pete', 'Grizzly']
// ******************************************************************


// 10---> ********* done **********
//True or False: array.slice() copies elements from original array and returns them as a new array.
//--->TRUE
let pet = "parrot"
let newPet = pet.slice(2); 
console.log(newPet) 
//  rrot
// ******************************************************************


// 11 ********* done **********
// Create a function named addToEnd that will add a passed in string to the end of a passed in 
//array. If element to be added is not a string, return the string "error - can only add strings 
//to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" 
//as arguments when testing.

let students = ["John", "Joe", "Jane", "Jessie"]
function addToEnd(name) {
  if ( typeof name === 'string' ) {
  	students.push(name)
  	return students
  } else {
  	return `error - can only add strings to an array`
  }
}
addToEnd("Ryan")
console.log(students) // (5) ['John', 'Joe', 'Jane', 'Jessie', 'Ryan']
// ******************************************************************


// 12  ********* done **********
//Create a function named addToStart that will add a passed in string to the start of a passed in array. 
//If element to be added is not a string, return the string "error - can only add strings to an array". 
//Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when 
//testing.

function addToStart (name) {
	if ( typeof name === 'string' ) {
		students.unshift(name)
		return students
	} else 
		return `error - can only add strings to an array`
}
addToStart("Tess")
console.log(students) // (6) ['Tess', 'John', 'Joe', 'Jane', 'Jessie', 'Ryan']
// ******************************************************************

// 13   ********* done **********
//Create a function named elementChecker that will check a passed in array if at least one of its elements 
//has the same value as a passed in argument. If array is empty, return the message "error - passed in 
//array is empty". Otherwise, return a boolean value depending on the result of the check. Use the 
//students array and the string "Jane" as arguments when testing.
function elementChecker (name) {
	 if ( name === ""  ) {
 		return `error - passed in array is empty`
 	 }
 	 else if (students.includes(name)) {
 		return true
 	 }

 	 else
 		return false
}
elementChecker("Jane") //true
//elementChecker("Karen") 
//elementChecker("")
// ******************************************************************


// 14  ---- in progress
/*
Create a function named checkAllStringsEnding that will accept a passed in array and a character. 
The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/


function checkAllStringsEnding (myArray, lastLetter) {

	for (let i = 0; i < myArray.length; i++) {
		if (typeof myArray[i] !== string) {
			return `error - all array elements must be strings`
		}
		else if (myArray === 0) {
			return `error - array must NOT be empty`
	    }
	    else if (typeof lastLetter !== string) {
			return `error - 2nd argument must be of data type string`
	    }
	    else if (lastLetter.length > 1) {
	    	return `error - 2nd argument must be a single character`
	    }
	    else if (myArray[i].endsWith(lastLetter)) {
	    	return true
	    }
	    else 
	    	return false
	}		
}
checkAllStringsEnding (students,"e") 


// 15
//Create a function named stringLengthSorter that will take in an array of strings as its argument 
//and sort its elements in an ascending order based on their lengths. If at least one element is 
//not a string, return "error - all array elements must be strings". Otherwise, return the sorted 
//array. Use the students array to test.



// 16   ---- in progress
/*
Create a function named startsWithCounter that will take in an array of strings and a single character. 
The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/
function startsWithCounter(name, char) {
	if (name === "" && char ==="") {
		console.log("error - array must NOT be empty")
	}
}
startsWithCounter()


// 17 ********* done **********
//Create a function named randomPicker that will take in an array and output any one of its 
//elements at random when invoked. Pass in the students array as an argument when testing.

function randomPicker() {
	let randomNum = Math.trunc(Math.random()*4)
	console.log(students[randomNum])
}
// ******************************************************************
